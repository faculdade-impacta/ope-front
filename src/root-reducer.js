import { combineReducers } from 'redux';
import { sessionReducer } from 'redux-react-session';
import { appReducer } from "./app/root/app.reducer";
import { firebaseReducer } from 'react-redux-firebase';

export default combineReducers({
  firebase: firebaseReducer,
  session: sessionReducer,
  app: appReducer
})


