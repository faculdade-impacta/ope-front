import axios from 'axios';
import { Constants } from "../config/constants";
import { getCurrentUser } from "./sessionServices";

const http = axios.create ({
  baseURL: Constants.api,
  timeout: 8000,
  headers: {'Content-Type': 'application/json'},
});

http.interceptors.request.use (
  (config) => {
    let token = getCurrentUser();
    if(token) config.headers.Authorization = `Bearer ${token}`; 
    return config;
  },
  (error) => Promise.reject(error)
);

export default http;