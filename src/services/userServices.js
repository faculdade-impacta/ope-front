import Amplify, { Auth } from 'aws-amplify';
import firebase from 'firebase';
import { sessionService } from 'redux-react-session';
import http from './httpServices';

Amplify.configure({
    identityPoolId: 'us-west-2:c3d2c5ff-e491-42a3-a745-32678f0493a9',
    region: 'us-west-2',
    identityPoolRegion: 'us-west-2',
    userPoolId: 'us-west-2_RDDQAuFFE',
    userPoolWebClientId: '6hr8b87m1fsrnkvvevouac69sf'
});

export const sessionRecovery = () => {
    return new Promise((resolve, reject) => {
        firebase.auth().onAuthStateChanged(currentUser => {
            if (!currentUser) console.log("Sessão não recuperada");
            startSession(firebase.auth().currentUser.ra).then(resolve).catch(reject);
        });
    });
};
// return new Promise((resolve, reject) => {
//     Auth.currentSession()
//         .then(
//             res => {
//                 sessionService.saveSession(res.accessToken)
//                     .then(() => {
//                         localStorage.setItem('caas_id_token', res.idToken);
//                         http.get('/user').then(
//                             user => {
//                                 sessionService.saveUser(user)
//                                 .then(resolve)
//                                 .catch(reject)
//                             }
//                         )
//                 }).catch(reject)
//             })
//         .catch(reject);
// });
// };

export const userSignUp = (newUser) => {
    return new Promise((resolve, reject) => {
        firebase.auth().createUserWithEmailAndPassword(newUser.email, newUser.password)
            .then(
                () => {
                    firebase.auth().onAuthStateChanged(currentUser => {
                        if (currentUser) {
                            currentUser.updateProfile({
                                displayName: newUser.name
                            }).catch(reject);
                            firebase.auth().currentUser.sendEmailVerification().then().catch(reject);
                            firebase.auth().currentUser.reload();
                            startSession(firebase.auth().currentUser.ra, firebase.auth().currentUser).then(resolve).catch(reject);
                            createUser(firebase.auth().currentUser, 0);
                        } else reject("Erro de autenticação");
                    });
                }).catch(reject);
    });
};

export const facebookSignIn = () => {
    const provider = new firebase.auth.FacebookAuthProvider();
    provider.addScope('public_profile, email, user_birthday, user_location, user_gender');
    return new Promise((resolve, reject) => {
        firebase.auth().signInWithPopup(provider).then(
            () => {
                let _user = firebase.auth().currentUser;
                _user = { ..._user, photoURL: _user.photoURL.concat('?type=large') }
                startSession(firebase.auth().currentUser.ra, firebase.auth().currentUser).then(resolve).catch(reject);
                createUser(_user, 0);
            }).catch(reject);
    })
};

export const googleSignIn = () => {
    return new Promise((resolve, reject) => {
        let provider = new firebase.auth.GoogleAuthProvider();
        provider.addScope('profile');
        provider.addScope('email');
        firebase.auth().signInWithPopup(provider).then(
            googleUser => {
                startSession(firebase.auth().currentUser.ra, firebase.auth().currentUser).then(resolve).catch(reject);
                createUser(firebase.auth().currentUser, 0);
            }).catch(reject);
    });
}

export const userSignIn = (credentials) => {
    return new Promise((resolve, reject) => {
        firebase.auth().signInWithEmailAndPassword(credentials.username, credentials.password)
            .then(
                authUser => {
                    console.log(authUser);
                    startSession(authUser.ra, authUser.user).then(resolve).catch(reject)
                })
            .catch(reject);
    })
};

export const startSession = (token) => {
    return new Promise((resolve, reject) => {
        // console.log("Vai começar a putaria: ", token)
        sessionService.saveSession(token)
            .then(() => {
                localStorage.setItem('caas_id_token', token);
                http.get('/user').then(
                    user => {
                        if (!user.data[0]) {
                            reject("Usuário não identificado");
                        }
                        sessionService.saveUser(user.data[0])
                            .then(resolve)
                            .catch(reject)
                    }
                )
            }).catch(reject);
    });
};

export const userLogout = () => {
    sessionService.deleteSession();
    sessionService.deleteUser();
    localStorage.setItem('caas_id_token', '');
    return firebase.auth().signOut().then().catch(console.log);
};

export const changePassword = (userInfo) => {
    return new Promise((resolve, reject) => {
        if (userInfo.old_password && userInfo.new_password) {
            firebase.auth().currentUser.updatePassword(userInfo.new_password.toString).then(resolve).catch(reject);
            userLogout();
        } else reject()
    })
};

export const recoverPassword = (email) => {
    return email.length ? firebase.auth().sendPasswordResetEmail(email) : null;
}

const createUser = (user, role) => {
    const { displayName, photoURL, phoneNumber, email, uid } = user;
    const userFormatted = {
        uid: uid,
        full_name: displayName.toUpperCase(),
        profile_pic: photoURL ? photoURL : '',
        phone: phoneNumber ? phoneNumber : '',
        email: email,
        role: role
    }
    console.log(userFormatted)
    sessionService.saveUser(userFormatted)
    http.post('/user', userFormatted)
};

//--------- WIP
export const editUserInfo = (userInfo) => {
    return new Promise((resolve, reject) => {
        uploadAvatar(userInfo.avatar).then((avatar_id) => {


            let edit = avatar_id ?
                {
                    password: {
                        old_password: userInfo.old_password,
                        new_password: userInfo.new_password
                    },
                    infos: {
                        email: userInfo.email,
                        phone_number: userInfo.phone_number.replace(/[^0-9.+]/g, ""),
                        name: userInfo.name,
                        gender: 'M',
                        birthdate: userInfo.birthdate,
                        address: userInfo.address,
                        "custom:state": userInfo.userState,
                        "custom:city": userInfo.userCity,
                        "custom:neighborhood": userInfo.userNeighborhood,
                        "custom:complete_register": '1',
                        "custom:avatar_id": avatar_id.imageUrl.split('/')[avatar_id.imageUrl.split('/').length - 1]
                    }
                } :
                {
                    password: {
                        old_password: userInfo.old_password,
                        new_password: userInfo.new_password
                    },
                    infos: {
                        email: userInfo.email,
                        phone_number: userInfo.phone_number.replace(/[^0-9.+]/g, ""),
                        name: userInfo.name,
                        gender: 'M',
                        birthdate: userInfo.birthdate,
                        address: userInfo.address,
                        "custom:state": userInfo.userState,
                        "custom:city": userInfo.userCity,
                        "custom:neighborhood": userInfo.userNeighborhood,
                        "custom:complete_register": '1',
                    }
                }

            Auth.currentAuthenticatedUser()
                .then(
                    user => {
                        Auth.updateUserAttributes(user, edit.infos)
                            .then(res => {
                                if (edit.password.old_password && edit.password.new_password) {
                                    Auth.changePassword(user, edit.password.old_password, edit.password.new_password)
                                        .then(
                                            res => {
                                                edit.infos.phone_number = edit.infos.phone_number.replace(/\+55/g, "")
                                                updateUser(edit.infos);
                                                resolve(res);
                                                userLogout();
                                            }
                                        ).catch(reject);
                                } else {
                                    edit.infos.phone_number = edit.infos.phone_number.replace(/\+55/g, "")
                                    updateUser(edit.infos);
                                    resolve(res);
                                    userLogout();
                                }
                            })
                            .catch(reject)
                    }).catch(reject)
        })
    });

};

const updateUser = (user) => {
    http
        .put('/user', user)
        .then().catch(console.log);
}

const uploadAvatar = (avatar) => {
    let data = new FormData();
    data.append('file', avatar);
    return http
        .post('/user/avatar', data)
        .then((res) => {
            console.log(res)
            return res.data;
        }
        )
        .catch((err) =>
            console.log(err)
        );
}