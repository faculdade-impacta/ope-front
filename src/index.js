import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router'
import { history, store } from './store'
import { sessionService } from 'redux-react-session'
import App from './app/root';
import { sessionRecovery } from './services/userServices'
import { checkApi } from './services/sessionServices'
import 'sanitize.css/sanitize.css'
import 'bootstrap/dist/css/bootstrap.css';
import './index.scss';

const target = document.querySelector('#root')
render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <div>
        <App />
      </div>
    </ConnectedRouter>
  </Provider>,
  target
)

const options = { refreshOnCheckAuth: true, redirectPath: '/', driver: 'COOKIES' };

sessionService.initSessionService(store, options)
  .then(() => {
    checkApi();
    sessionRecovery()
      .then().catch(() => console.log('Sessão não recuperada'))
  }).catch(console.log('Sessão não iniciada'));
