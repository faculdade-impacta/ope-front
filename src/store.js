import { createStore, applyMiddleware, compose } from 'redux'
import { connectRouter, routerMiddleware } from 'connected-react-router'
import { sessionService } from 'redux-react-session';
import thunk from 'redux-thunk'
import createHistory from 'history/createBrowserHistory'
import rootReducer from './root-reducer'
import firebase from "firebase";
import { reactReduxFirebase } from 'react-redux-firebase';
import 'firebase/firestore'; 
import 'firebase/functions';

export const history = createHistory()

const initialState = {}
const firebaseConfig = {
  apiKey: "AIzaSyDom_73t2avVtFg--QZpDGT7QQICNM70dc",
  authDomain: "conline-auth.firebaseapp.com",
  databaseURL: "https://conline-auth.firebaseio.com",
  projectId: "conline-auth",
  storageBucket: "conline-auth.appspot.com",
  messagingSenderId: "966053097800",
};
const enhancers = [ 
  reactReduxFirebase(firebase, firebaseConfig)
];
const middleware = [thunk, routerMiddleware(history)]

if (process.env.NODE_ENV === 'development') {
  const devToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION__

  if (typeof devToolsExtension === 'function') {
    enhancers.push(devToolsExtension())
  }
}

firebase.initializeApp(firebaseConfig);
firebase.auth().useDeviceLanguage();

const composedEnhancers = compose(
  applyMiddleware(...middleware),
  ...enhancers
)

export const store =  createStore(
  connectRouter(history)(rootReducer),
  initialState,
  composedEnhancers
)

sessionService.initSessionService(store); 
