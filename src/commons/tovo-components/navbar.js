import React from 'react';
import Link from "../components/link";
import Icon from '@material-ui/core/Icon';

class Navbar extends React.Component {
	render() {
		return (
			<nav className="navbar navbar-expand-lg navbar-light theme-nav fixed-top">
				<div className="container">
					{/* <Link className="navbar-brand nav-logo negativo" to="/"><img src="assets/images/app/marca-negativo.svg" alt="logo" /></Link>
					<Link className="navbar-brand nav-logo positivo" to="/"><img src="assets/images/app/marca-positivo.svg" alt="logo" /></Link> */}
					<Link className="navbar-brand nav-logo negativo" to="/"><img src="assets/images/app/marca-negativo.svg" alt="logo" /></Link>
					<Link className="navbar-brand nav-logo positivo" to="/"><img src="assets/images/app/marca-positivo.svg" alt="logo" /></Link>
					<button className="navbar-toggler" id="toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<i className="fa fa-bars"></i>
					</button>
					<div className="collapse navbar-collapse default-nav" id="navbarSupportedContent">
						<ul className="navbar-nav ml-auto" id="mymenu">
							<li className="nav-item">
								<Link className="nav-link" to="/#sobre">Sobre</Link>
							</li>
							{/* <li className="nav-item">
								<Link className="nav-link" to="/#como-funciona">Como funciona</Link>
							</li> */}
							{/* <li className="nav-item">
								<Link className="nav-link" to="#" data-menuanchor="contact">Contato</Link>
							</li> */}
							<li className="nav-item">
								<Link className="nav-link" to="/agendar-cuidador">Solicitar Cuidador</Link>
							</li>
							<li className="nav-item">
								<Link className="nav-link btn-third join-us" to="/cadastro-cuidador">Seja um Profissional</Link>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		);
	}
}

export default Navbar;