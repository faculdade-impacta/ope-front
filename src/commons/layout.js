import styled from "styled-components";
import { Container, Row, Col, FormGroup } from "reactstrap";
import { Paper, Grid } from '@material-ui/core'

export const StructureLayout = {
    Container: styled(Container)`
    
    `,
    Row: styled(Row)`
        
    `,
    Col: styled(Col)`
       
    `,
    FormGroup: styled(FormGroup)`
       
    `,
    Paper: styled(Paper)`
    
    `,
    Grid: styled(Grid)`
        margin: 0px !important;
    `,
}

export const TemplateLayout = {

}

export const Fonts = {
    defaultFont: "'Raleway', sans-serif",
    bannerTitle: styled.h1`
        font-family: 'Raleway', sans-serif;
        color: #424242;
        font-weight: 400;
        margin: 0;
    `,
    pageTitle: styled.h1`
        font-family: 'Raleway', sans-serif;
        color: "#00BCD4";
        font-weight: 400;

        text-align: center;
    `,

    simpleLink: styled.a`
        font-family: 'Raleway', sans-serif;
        &:hover, &:focus {
            text-decoration: none;
            outline: none;
        }
    `,
}

export const Colors = {
    white: "#ffff",
    primaryColor: "#00BCD4",
    sencodaryColor: "#FF005C",
    shadows: {
        primary: "0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)"
    }
}