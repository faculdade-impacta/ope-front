import React, { Component } from "react";

class AlertModal extends Component {
    render() {
        return (
            <div class="modal fade" id="alert-modal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">{this.props.title}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            {this.props.message}
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Ok</button>
                            {/* <button type="button" onClick={this.props.onClick} class="btn btn-primary" data-dismiss="modal">Sim</button> */}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
} export default AlertModal;

