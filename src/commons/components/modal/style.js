import styled from 'styled-components';
import InputSimple from "../../components/simple-input";
import DefaultButton from "../../components/default-button";
import { device } from '../../devices';

export const ModalElements = {

    backdropStyle: styled.div`
        position: fixed; 
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        background-color: rgba(0,0,0,0.5);
        display: flex;
        align-items: center;
        justify-content: center;
        z-index: 1031;
        @media ${ device.mobile} {
            overflow-y: scroll;
        }
    `,
    modalStyle: styled.div`
        background-color: #fff;
        border-radius: 15px;
        margin: auto;
        padding: 30;
        z-index: 9998;
        padding: 30px 50px;
        @media ${ device.mobile} {
            padding: 0px;
        }
    `,
    contentStyle: styled.div`
    width: 100%;
    background-color: #fff;
        margin: auto;
        z-index: 9999;
        display: flex;
        flex-direction: column;
        justify-content: start;
        border-radius: 1rem !important;
        @media ${ device.mobile} {
            padding: 0px;
        }
    `
}