import React from 'react';


class Feature extends React.Component {
   render() {

      return (
         <section id="feature" className="feature">
            <div className="container">
               <div className="row">
                  <div className="feature-phone">
                     <img src="assets/images/222.png" className="img-fluid" alt="" />
                  </div>
                  {/* <div className="col-lg-10"> */}
                  <div className="row">
                     <div className="col-sm-12 mrgn-md-top">
                        <h2 className="title blue">Como<span> funciona?</span></h2>
                     </div>
                     <div className="col-12 col-md-6">
                        <p>
                           A cuiador online conecta você a diversos profissionais que prestam serviços de <strong>acompanhantes</strong> e <strong>cuidadores</strong>.
                              De forma simples você <strong>agenda</strong> um profissional para te acompanhar em um exame ou para cuidar de você quando você precisar.</p>
                        <div>
                           <h3 className="blue">Veja como é fácil!</h3>
                           <button className="btn btn-primary" type="submit">Solicitar Cuidador</button>
                        </div>
                     </div>
                     <div className="col-12 col-md-2 sm-m-top"></div>
                     <div className="col-12 col-md-4 sm-m-top">
                        <ul className="feature-style">
                           <li>
                              <h3 className="blue"><span>🗓</span> Solicite um profissional</h3>
                              <p>Agende um profissional pelo site, informando o dia e horário.</p>
                           </li>
                           <li>
                              <h3 className="blue"><span>👩🏻‍⚕️</span> Deixa com a gente!</h3>
                              <p>Disponibilizaremos um profissional aprovado por você na data e local definido.</p>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
            {/* </div> */}
         </section>
      );
   }
}


export default Feature;