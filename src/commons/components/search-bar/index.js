import React, { Component } from "react";
import { Input } from "reactstrap";
class searchBar extends Component {
    render() {
        return (
            <Input
                required={this.props.required}
                placeholder={this.props.placeholder}
                onChange={this.props.onChange}
                name={this.props.name}
                value={this.props.value}
            />
        );
    }
}

export default searchBar;