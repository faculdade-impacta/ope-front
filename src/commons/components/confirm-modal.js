import React, { Component } from "react";

class ConfirmModal extends Component {
    render() {
        return (
            <div class="modal fade" id={this.props.serviceId} tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Deletar Serviço</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            Deseja realmente apagar esse Serviço?
                                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Não</button>
                            <button type="button" onClick={this.props.onClick} class="btn btn-primary" data-dismiss="modal">Sim</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
} export default ConfirmModal;

