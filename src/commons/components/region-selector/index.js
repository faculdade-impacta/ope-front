import React, { Component } from "react";
import { MenuItem, FormControl } from "@material-ui/core"
import { SelectorElements } from "./style";
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    icon: {
        display: "none",
    },
    select: {
        paddingLeft: "32px",
        textAlign: "center",
    }
});

class RegionSelector extends Component {
    render() {
        const {classes} = this.props;
        return (
            <div>
                <FormControl >
                    <SelectorElements.Select
                        value={ this.props.value }
                        onChange={ this.props.onChange }
                        displayEmpty
                        disableUnderline={true}
                        inputProps={{
                            name: this.props.name,
                            id: 'region-selector',
                            classes: {
                                icon: classes.icon
                            },
                            className: classes.select
                        }}
                    >
                        <MenuItem value="" disabled>
                            { this.props.message }
                        </MenuItem>
                        <MenuItem value="SP">SP</MenuItem>
                        <MenuItem value="RJ">RJ</MenuItem>
                        <MenuItem value="MG">MG</MenuItem>
                    </SelectorElements.Select>
                </FormControl>
            </div>
        );
    }
}

export default withStyles(styles)(RegionSelector);