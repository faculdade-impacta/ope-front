import styled from "styled-components";
import { Select } from "@material-ui/core"

const styledSelect = "StyledSelect";

export const SelectorElements = {
    Select: styled(Select).attrs({
        classes: { styledSelect }
    })`
        .${ styledSelect } {
            padding: 0px;
        }
        background-color: #673AB7;
        border-radius: 25px;
        width: 120px;
        height: 32px;
        color: #FFF !important;
    `
}