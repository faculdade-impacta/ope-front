import React, { Component } from "react";
import { InputElements } from "./style";

class InputPassword extends Component {
    constructor(props) {
        super(props)
        this.state = {
            showPassword: false
        }
        this.handleClickShowPassword = this.handleClickShowPassword.bind(this);
    }
    
    handleClickShowPassword = () => {
        this.setState({ showPassword: !this.state.showPassword });
    };

    render() {
        return(
            <InputElements.TextField
                placeholder={ this.props.placeholder } 
                value={ this.props.value } 
                onChange={ this.props.onChange }
                name={ this.props.name }
                type={ this.state.showPassword ? 'text' : 'password' }
                label={ this.props.label }
                margin={ this.props.margin || "normal" }
                variant={ this.props.variant || "filled" }
                fullWidth
                InputProps={{
                    endAdornment: (
                    <InputElements.InputAdornment 
                        variant={ this.props.variant || "filled" } 
                        position={ this.props.postion || "end" }>
                        <InputElements.IconButton
                            aria-label={ this.props.ariaLabel }
                            onClick={ this.handleClickShowPassword }
                        >
                        { this.state.showPassword ? <InputElements.VisibilityOff /> : <InputElements.Visibility />}
                        </InputElements.IconButton>
                    </InputElements.InputAdornment>
                    ),
                }}
            />
        ); 
    }  
} export default InputPassword;