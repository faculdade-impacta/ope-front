import styled from "styled-components";
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem
} from 'reactstrap';
import { Menu, MoreVert } from '@material-ui/icons';
import Avatar from '@material-ui/core/Avatar';
import defaultLink from "../../components/link";
import { Fonts } from '../../layout';

export const NavbarElements = {
    Wrap: styled.div`
        width: 100%;
        margin: 0;
        padding: 0;
        background-color: #3388DB;
        color: #ffffff;

    -webkit-box-shadow:0 0px 2px rgba(0,0,0,0.3), 0 0 20px rgba(0,0,0,0.05) inset;
       -moz-box-shadow:0 0px 2px rgba(0,0,0,0.3), 0 0 20px rgba(0,0,0,0.05) inset;
            box-shadow:0 0px 2px rgba(0,0,0,0.3), 0 0 20px rgba(0,0,0,0.05) inset;
    `,
    Container: styled.div`
        max-width: 1500px;
        margin: 0 auto;
    `,
    Navbar: styled(Navbar)``,
    NavbarBrand: styled(NavbarBrand)`
        color: rgba(255, 255, 255, 1)!important;
        font-family: ${Fonts.defaultFont};
        position: relative;
        background-repeat: no-repeat;
        background-size: 13vh;
        display: block;
        width: 135px;
        height: 30px;
    `,
    NavbarToggler: styled(NavbarToggler)`
        padding: 0!important;
        &:focus {
            outline: none;

        }
    `,
    Collapse: styled(Collapse)``,
    Nav: styled(Nav)``,
    NavItem: styled(NavItem)`
        padding: 0;
    `,
    NavLink: styled(NavLink)`
        color: rgba(255, 255, 255, 0.85)!important;
        &:hover {
            color: rgba(255, 255, 255, 1)!important;
        };
        font-family: ${Fonts.defaultFont};
        padding-right: 0.8rem !important;
        padding-left: 0.8rem !important;
    `,

    WrapProfileMenu: styled.div`
        display: flex;
        flex-direction: row;
        align-items: center;

          -webkit-touch-callout: none;
            -webkit-user-select: none;
             -khtml-user-select: none;
               -moz-user-select: none;
                -ms-user-select: none;
                    user-select: none;
    `,

    MenuIcon: styled(Menu)`
        fill: rgba(255, 255, 255, 1)!important;
    `,

    Avatar: styled(Avatar)`
    
    `,
    MoreIcon: styled(MoreVert)`
        fill: rgba(255, 255, 255, 1)!important;
        margin-left: -5px;
    `,
    UncontrolledDropdown: styled(UncontrolledDropdown)``,
    DropdownToggle: styled(DropdownToggle)``,
    DropdownMenu: styled(DropdownMenu)`
        position: absolute!important;
        left: -100px !important;
        width: 150px !important;
        padding: 10px !important;
    `,
    DropdownItem: styled(DropdownItem)`
        padding-right: 0px !important;
    `,
    Link: styled(defaultLink)`
        color: #000;
        &:hover, &:focus {
            color: #000;
            text-decoration: none;
            outline: none;
        }
    `,
}