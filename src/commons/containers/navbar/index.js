import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { StructureLayout } from "../../layout";
import { NavbarElements } from "./style";
import { userLogout } from "../../../services/userServices";
import Link from "../../components/link";
import NavBar from "../../tovo-components/navbar"
import { Constants } from "../../../config/constants"

class MenuNavbar extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false,
            session: {}
        };
    }
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
    render() {
        const { authenticated, user } = this.props.session;
        const abbreviation = user.full_name ? `${user.full_name.split(' ')[0].charAt(0).toUpperCase()}${user.full_name.split(' ')[1].charAt(0).toUpperCase() || ''}` : '';
        const complete_register = user.completed;
        const profileImage = user.profile_pic ? user.profile_pic : '';

        if (authenticated) return (
            <nav className="navbar navbar-expand-lg navbar-light theme-nav fixed-top">
                <div className="container">
                    {/* <Link className="navbar-brand nav-logo negativo" to="/"><img src="assets/images/app/marca-negativo.svg" alt="logo" /></Link>
					<Link className="navbar-brand nav-logo positivo" to="/"><img src="assets/images/app/marca-positivo.svg" alt="logo" /></Link> */}
                    <Link className="navbar-brand nav-logo negativo" to="/"><img src="assets/images/app/marca-negativo.svg" alt="logo" /></Link>
                    <Link className="navbar-brand nav-logo positivo" to="/"><img src="assets/images/app/marca-positivo.svg" alt="logo" /></Link>
                    <button className="navbar-toggler" id="toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i className="fa fa-bars"></i>
                    </button>
                    <div className="collapse navbar-collapse default-nav" id="navbarSupportedContent">
                        <ul className="navbar-nav ml-auto" id="mymenu">
                            <li className="nav-item">
                                <Link className="nav-link" to="/">Sobre</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/#como-funciona">Como funciona</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="#" data-menuanchor="contact">Contato</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/agendar-cuidador">Encontrar Cuidador</Link>
                                {/* <a className="nav-link" target="_blank" href="https://goo.gl/forms/t4zPZVtWvJRlgvMi2">Encontrar Cuidador</a> */}
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link btn-third join-us" to="/cadastro-cuidador">Seja um Profissional</Link>
                                {/* <a className="nav-link btn-third join-us" target="_blank" href="https://goo.gl/forms/2PPixT1x0NM0dPgu1">Seja um Profissional</a> */}
                            </li>
                            {/* {
                                complete_register === 0 ?
                                    <li className="nav-item"><Link className="nav-link btn-third join-us" to="/editar-perfil?professional=1">Seja um Profissional</Link></li>
                                    :
                                    <li className="nav-item"><Link className="nav-link btn-third join-us" to="/servicos">Meus Serviços</Link></li>
                            } */}
                            {/* <li className="nav-item nav-user-menu-mobile">
                                <Link className="nav-link" to="/editar-perfil">Editar Perfil</Link>
                            </li> */}
                            <li className="nav-item nav-user-menu-mobile" onClick={userLogout}>
                                <Link className="nav-link" to="#">Sair</Link>
                            </li>
                            <li className="nav-item nav-user-menu">
                                <ul>
                                    <NavbarElements.UncontrolledDropdown nav inNavbar>
                                        <NavbarElements.DropdownToggle nav>
                                            <NavbarElements.WrapProfileMenu>
                                                <NavbarElements.Avatar src={profileImage ? `${profileImage}` : ''}>
                                                    {
                                                        ((props) => profileImage ? '' : abbreviation)(this.props)
                                                    }
                                                </NavbarElements.Avatar>
                                                <NavbarElements.MoreIcon />
                                            </NavbarElements.WrapProfileMenu>
                                        </NavbarElements.DropdownToggle>
                                        <NavbarElements.DropdownMenu right>
                                            {/* <NavbarElements.DropdownItem>
                                                <NavbarElements.Link to="/editar-perfil">Editar Perfil</NavbarElements.Link>
                                            </NavbarElements.DropdownItem> */}
                                            <NavbarElements.DropdownItem onClick={userLogout}>
                                                Sair
                                            </NavbarElements.DropdownItem>
                                        </NavbarElements.DropdownMenu>
                                    </NavbarElements.UncontrolledDropdown>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        );
        return (
            <NavBar />
        );
    }
}


const mapStateToProps = state => ({
    session: state.session
});

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(MenuNavbar);