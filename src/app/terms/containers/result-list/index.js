import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from 'react-router-dom';
import { loadSearch } from "../../terms.actions";
import Profile from "../../components/profile";
import { DotLoader } from 'react-spinners';
import { cleanErrors } from '../../../root/app.actions';

class ResultList extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }


    componentDidMount() {
        this.props.cleanErrors();
    }

    render() {
        let { searchResult, loading, error } = this.props.app;
        let { authenticated } = this.props.session;

        if (loading) return (
            <div className="center-loader">
                <DotLoader
                    sizeUnit={"px"}
                    size={50}
                    color={'#25dde0'} />
            </div>
        )

        if (error) return (
            <h3>Erro</h3>
        );

        if (searchResult.length === 0) {
            return (
                <div className="row not-found-container">
                    <div className="col-md-8">
                        <div className="not-found-message">
                            <p className="line">Oops, nenhum resultado encontrado =(</p>
                            <p className="line">Por favor refaça sua pesquisa.</p>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <img className="enfermeira-triste" src="assets/images/app/enfermeira-triste.svg" alt="" />
                    </div>
                </div>
            )
        }

        if (searchResult.length > 0) {
            return (
                <div className="container result-container">
                    <div className="row"> {
                        searchResult
                            .map((element, index) =>
                                <Profile
                                    categories={element.categories}
                                    key={index}
                                    avatar_id={element.user_location.user.profile_pic}
                                    name={element.user_location.user.full_name}
                                    authenticated={authenticated}
                                    email={element.user_location.user.email || ''}
                                    phone={element.user_location.user.phone || ''}
                                    location={`${element.user_location.location.city} - ${element.user_location.location.uf.toUpperCase()}`}
                                    title={element.title}
                                    resume={element.resume}
                                />
                            )
                    }
                    </div>
                </div>
            );
        }
        return <Profile />
    }
};

const mapStateToProps = state => ({
    app: state.app,
    session: state.session
})

const mapDispatchToProps = dispatch => bindActionCreators({ loadSearch, cleanErrors }, dispatch)

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ResultList));