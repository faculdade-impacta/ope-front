import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from 'react-router-dom';
import { loadSearch } from "../../terms.actions";
import { registerSearch } from "../../../../services/appServices";
import { withStyles } from '@material-ui/core/styles';
import { Constants } from '../../../../config/constants';
import Autosuggest from 'react-autosuggest';

const styles = theme => ({
    inputClass: {
        backgroundColor: '#FFF',
        paddingLeft: '38px',
        width: '100%',
        height: '48px',
    }
});

const getSuggestions = value => {
    const inputValue = value.trim().toLowerCase();
    const inputLength = inputValue.length;

    return inputLength === 0 ? [] : Constants.UF.filter(lang =>
        lang.nome.toLowerCase().slice(0, inputLength) === inputValue
    );
};

const getSuggestionValue = suggestion => suggestion.nome;

const renderSuggestion = suggestion => (
    <div>
        {suggestion.nome}
    </div>
);

class SearchBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchParameters: {
                searchTerm: this.props.app.searchParameters.searchTerm ? this.props.app.searchParameters.searchTerm : '',
                onLocation: this.props.app.searchParameters.onLocation ? this.props.app.searchParameters.onLocation : ''
            },
            value: "",
            suggestions: []
        };
        this.sendQuery = this.sendQuery.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.onSuggestionsFetchRequested = this.onSuggestionsFetchRequested.bind(this);
        this.onSuggestionsClearRequested = this.onSuggestionsClearRequested.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    sendQuery(event) {
        event.preventDefault();
        if (this.state.searchParameters.onLocation || this.state.searchParameters.searchTerm) {
            this.props.registerSearch(this.state.searchParameters);
        }
        this.props.loadSearch(this.state.searchParameters.onLocation, this.state.searchParameters.searchTerm);
    }

    handleInputChange(e) {
        this.setState({
            searchParameters: {
                ...this.state.searchParameters, [e.target.name]: e.target.value.toString().trim()
            }
        })
    }

    onChange = (event, { newValue }) => {
        this.setState({
            value: newValue,
            searchParameters: {
                onLocation: newValue,
                searchTerm: this.state.searchParameters.searchTerm
            }
        });
    };

    onSuggestionsFetchRequested = ({ value }) => {
        this.setState({
            suggestions: getSuggestions(value)
        });
    };

    onSuggestionsClearRequested = () => {
        this.setState({
            suggestions: []
        });
    };

    render() {
        const { classes } = this.props;
        const { value, suggestions } = this.state;

        const inputProps = {
            placeholder: 'Qual a Região?',
            value,
            onChange: this.onChange
        };
        return (
            <form onSubmit={this.sendQuery}>
                <div className="form-group theme-form row search-container" id="search-form">
                    <div className="col-sm-12 col-md-5 search-container">
                        <span className="result-search-title">Resultados para:</span>
                        <input
                            name="searchTerm"
                            onChange={this.handleInputChange}
                            required={true}
                            type="text"
                            className="form-control"
                            id=""
                            placeholder="O que você procura?"
                            value={this.state.searchParameters.searchTerm ? this.state.searchParameters.searchTerm : (this.props.app.searchParameters.searchTerm && typeof (this.state.searchParameters.searchTerm) === 'undefined' ? this.props.app.searchParameters.searchTerm : '')}
                        />
                    </div>
                    <div className="col-sm-12 col-md-4 search-container">
                        <span className="region-title">Próximo a:</span>
                        <Autosuggest
                            suggestions={suggestions}
                            onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                            onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                            getSuggestionValue={getSuggestionValue}
                            renderSuggestion={renderSuggestion}
                            inputProps={inputProps}
                        />
                        {/* <input
                            name="onLocation"
                            onChange={this.handleInputChange}
                            type="text"
                            className="form-control region-elem"
                            id=""
                            placeholder="Qual a região?"
                            value={this.state.searchParameters.onLocation ? this.state.searchParameters.onLocation : (this.props.app.searchParameters.onLocation && typeof (this.state.searchParameters.onLocation) === 'undefined' ? this.props.app.searchParameters.onLocation : '')}
                        /> */}
                    </div>
                    <div className="form-button col-sm-12 col-md-3">
                        <button type="submit" className="btn btn-primary">Buscar</button>
                    </div>
                </div>
            </form>
        );
    }
}


const mapStateToProps = state => ({
    app: state.app,
    session: state.session
})

const mapDispatchToProps = dispatch => bindActionCreators({ registerSearch, loadSearch }, dispatch)

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(SearchBar)));