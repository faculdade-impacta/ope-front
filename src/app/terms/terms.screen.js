import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from 'react-router-dom';
import { loadSearch } from './terms.actions';
import MenuNavbar from "../../commons/containers/navbar";
import SearchBar from "./containers/search-bar";
import ResultList from "./containers/result-list";
import { Wrappers } from "./terms.style";
import { cleanErrors } from '../root/app.actions';
import { StructureLayout } from "../../commons/layout";

class TermsPage extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        this.props.cleanErrors();
        const { query, region } = this.props.match.params;
        this.props.loadSearch(region, query);
    }

    render() {
        return (
            <div className="app-terms">
                <MenuNavbar />
                <Wrappers.header>
                </Wrappers.header>
                <StructureLayout.Row className="about-agendamento">
                    <StructureLayout.Container>
                        <StructureLayout.Row >
                            <StructureLayout.Col md="8" lg="10">
                                <div>
                                    <strong><h2>Termos de uso</h2></strong>
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vitae elit lacus. Etiam sollicitudin faucibus dictum. Phasellus enim eros, varius non eros molestie, consectetur hendrerit quam. Sed pharetra consectetur sem ut rhoncus. Curabitur eget sagittis lectus. In sed fermentum erat. Phasellus tortor nulla, feugiat in risus vitae, commodo consectetur mi. Praesent fermentum porta dignissim. Maecenas lacinia nunc quis fringilla scelerisque. Integer eu hendrerit turpis.
                                    </p>

                                    <p>
                                        Pellentesque suscipit quis ex in iaculis. In a justo et augue fringilla fermentum. Donec luctus, mauris et sodales eleifend, nulla tortor malesuada velit, ut auctor justo ante vel sem. Praesent vel lacinia urna. Suspendisse potenti. Suspendisse potenti. Morbi aliquam eget ante facilisis dignissim. Phasellus iaculis scelerisque dictum. Aenean bibendum, ex vel consectetur imperdiet, metus nisl sagittis libero, id fringilla tellus libero quis massa. Phasellus bibendum nisi velit, quis interdum metus consequat nec. Integer maximus sit amet est in laoreet. Cras ac erat eu nibh mattis efficitur. Cras nec ante sed tellus elementum tincidunt ut non sem. Sed in placerat purus. Nullam tristique odio eget sodales pellentesque.
                                    </p>
                                    <p>
                                        Donec et tortor sit amet tellus venenatis mattis. Pellentesque egestas eleifend nisl, ac consequat dolor dapibus at. Nulla consequat consectetur ornare. Pellentesque eu arcu nibh. Vestibulum viverra nec mauris quis commodo. Donec libero purus, dapibus eget tristique vel, consequat in tellus. Suspendisse rutrum nisi elementum, imperdiet diam ut, blandit purus. Pellentesque vitae mi sapien. Nulla dapibus congue condimentum. Vivamus pharetra velit ac erat porttitor, sed mollis mauris molestie. In iaculis bibendum justo nec luctus. Vivamus congue scelerisque risus, eget congue felis ornare a. Proin et volutpat ex.
                                    </p>
                                    <p>
                                        Morbi fermentum condimentum tellus et viverra. Aliquam erat volutpat. Etiam vulputate non nibh vitae elementum. Interdum et malesuada fames ac ante ipsum primis in faucibus. In orci nisl, vehicula quis malesuada eu, tristique eu velit. Praesent vel mauris ex. Aliquam rhoncus neque quis quam mollis varius. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed cursus elit vel imperdiet euismod. Pellentesque vitae convallis erat. Curabitur vel ullamcorper ante.
                                    </p>
                                </div>
                            </StructureLayout.Col>
                        </StructureLayout.Row>
                        <div>
                            <div>


                            </div>
                        </div>
                    </StructureLayout.Container>
                </StructureLayout.Row>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    app: state.app,
})
const mapDispatchToProps = dispatch => bindActionCreators({ loadSearch, cleanErrors }, dispatch);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(TermsPage));
