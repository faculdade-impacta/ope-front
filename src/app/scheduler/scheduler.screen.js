import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from 'react-router-dom';
import { loadSearch } from './scheduler.actions';
import MenuNavbar from "../../commons/containers/navbar";
import { Wrappers } from "./scheduler.style";
import { cleanErrors } from '../root/app.actions';
import { StructureLayout } from "../../commons/layout";
import Footer from "../../commons/tovo-components/footer";

class SchedulerPage extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        this.props.cleanErrors();
        const { query, region } = this.props.match.params;
        this.props.loadSearch(region, query);
    }

    render() {
        return (
            <div className="app-scheduler-form">
                <MenuNavbar />
                <Wrappers.header>
                </Wrappers.header>
                <StructureLayout.Container className="form-container">
                    <StructureLayout.Row >
                        <StructureLayout.Col md="12" lg="12" className="wrap-form">
                            <form action="https://docs.google.com/forms/d/e/1FAIpQLSdNTwgMxylInWPLogMwC6ay9aNhgjTS_MJMwFJqZ4bIpi75zg/formResponse"
                                target="hidden_iframe"
                                id="bootstrapForm"
                                method="POST">
                                <fieldset>
                                    <h3>Solicitar cuidador<br></br><br></br>
                                        <p><h5>Como funciona?</h5></p>
                                        <small>
                                            <p>Nós da Cuidador Online iremos encontrar a melhor alternativa para o seu caso.</p>
                                            <p>Entendemos que cada cuidado é um cuidado diferente, portanto queremos entender como podemos ajudá-lo!</p>
                                            <p>Preencha o formulário abaixo para que possamos encontrar a melhor opção para a sua necessidade.                                Enviaremos um email de confirmação e entraremos em contato para seguirmos com os próximos passos :).</p>
                                        </small>
                                    </h3>
                                </fieldset>
                                <br></br><br></br>
                                {/* <!-- Field type: "dropdown" id: "926888572" --> */}
                                <fieldset>
                                    <span for="926888572">O paciente é:</span>
                                    <div class="form-group">
                                        <select id="926888572" name="entry.129727569" class="form-control">
                                            <option value=""></option>
                                            <option value="Criança">Criança</option>
                                            <option value="Adulto(a)">Adulto(a)</option>
                                            <option value="Idoso(a)">Idoso(a)</option>
                                        </select>
                                    </div>
                                </fieldset>

                                {/* <!-- Field type: "checkboxes" id: "1434993109" --> */}
                                <fieldset>
                                    <legend for="1434993109">Qual é o tipo de serviço é necessário?</legend>
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="entry.1798142712" value="Companhia" />
                                                Companhia
                                        </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="entry.1798142712" value="Acompanhamento para consulta" />
                                                Acompanhamento para consulta
                                        </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="entry.1798142712" value="Acompanhamento em atividades rotineiras (ex: mercado, passeio)" />
                                                Acompanhamento em atividades rotineiras (ex: mercado, passeio)
                                        </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="entry.1798142712" value="Atividades domésticas" />
                                                Atividades domésticas
                                        </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="entry.1798142712" value="Banho" />
                                                Banho
                                        </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="entry.1798142712" value="Refeição" />
                                                Refeição
                                        </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="entry.1798142712" value="Administração de medicação" />
                                                Administração de medicação
                                        </label>
                                        </div>
                                    </div>
                                </fieldset>

                                {/* <!-- Field type: "dropdown" id: "638032958" --> */}
                                <fieldset>
                                    <legend for="638032958">Onde o paciente se encontra?</legend>
                                    <div class="form-group">
                                        <select id="638032958" name="entry.2010979627" class="form-control">
                                            <option value=""></option>
                                            <option value="Residência - acompanhado">Residência - acompanhado</option>
                                            <option value="Residência - sozinho">Residência - sozinho</option>
                                            <option value="Hospital">Hospital</option>
                                            <option value="Casa de repouso">Casa de repouso</option>
                                        </select>
                                    </div>
                                </fieldset>

                                {/* <!-- Field type: "dropdown" id: "2025363240" --> */}
                                <fieldset>
                                    <span for="2025363240">Qual a frequência do serviço?</span>
                                    <div class="form-group">
                                        <select id="2025363240" name="entry.1348966337" class="form-control">
                                            <option value=""></option>
                                            <option value="Uma única vez">Uma única vez</option>
                                            <option value="Aos finais de semana">Aos finais de semana</option>
                                            <option value="Mensalmente">Mensalmente</option>
                                        </select>
                                    </div>
                                </fieldset>


                                {/* <!-- Field type: "paragraph" id: "729548751" --> */}
                                <fieldset>
                                    <span for="729548751">Informações Adicionais</span>
                                    <div class="form-group">
                                        <textarea id="2050617292" name="entry.2050617292" class="form-control" ></textarea>
                                    </div>
                                </fieldset>


                                {/* <!-- Field type: "date" id: "584115594" --> */}
                                <fieldset>
                                    <span for="584115594">Informe a data que você irá precisar do profissional</span>
                                    <div class="form-group">
                                        <input type="date" id="160297191_date" placeholder="20/02/2019" class="form-control" />
                                    </div>
                                </fieldset>
                                <br></br>
                                <hr></hr>
                                <br></br>
                                {/* <!-- Field type: "section" id: "1346356672" --> */}
                                <fieldset>
                                    <span for="1346356672"><h3>Informações do solicitante</h3></span>
                                    <div class="form-group">
                                        <p class="help-block">Preencha o formulário para finalizar a solicitação do cuidador.</p>
                                    </div>
                                </fieldset>


                                {/* <!-- Field type: "short" id: "1780192073" --> */}
                                <fieldset>
                                    <span for="1780192073">Nome completo:</span>
                                    <div class="form-group">
                                        <input id="1917816024" type="text" name="entry.1917816024" class="form-control" />
                                    </div>
                                </fieldset>


                                {/* <!-- Field type: "short" id: "1129983233" --> */}
                                <fieldset>
                                    <span for="1129983233">Email:</span>
                                    <div class="form-group">
                                        <input id="1885689295" type="text" name="entry.1885689295" class="form-control" />
                                    </div>
                                </fieldset>


                                {/* <!-- Field type: "short" id: "1780728092" --> */}
                                <fieldset>
                                    <span for="1780728092">Número do celular (DDD + Número):</span>
                                    <div class="form-group">
                                        <input id="318818201" type="text" name="entry.318818201" class="form-control" />
                                    </div>
                                </fieldset>


                                {/* <!-- Field type: "short" id: "141704333" --> */}
                                <fieldset>
                                    <span for="141704333">CEP:</span>
                                    <div class="form-group">
                                        <input id="2089723922" type="text" name="entry.2089723922" class="form-control" />
                                    </div>
                                </fieldset>

                                <input type="hidden" name="fvv" value="1" />
                                <input type="hidden" name="fbzx" value="6693765368459528730" />
                                {/* <!--
                                CAVEAT: In multipages (multisection) forms, *pageHistory* field tells to google what sections we've currently completed.
                                This usually starts as "0" for the first page, then "0,1" in the second page... up to "0,1,2..N" in n-th page.
                                Keep this in mind if you plan to change this code to recreate any sort of multipage-feature in your exported form.
                                We're setting this to the total number of pages in this form because we're sending all fields from all the section together.
                                    --> */}
                                <input type="hidden" name="pageHistory" value="0,1" />

                                <input class="btn btn-primary" type="submit" value="Enviar"></input>
                            </form>
                        </StructureLayout.Col>
                    </StructureLayout.Row>
                </StructureLayout.Container>
                <Footer />
            </div>
        )
    }
}

const mapStateToProps = state => ({
    app: state.app,
})
const mapDispatchToProps = dispatch => bindActionCreators({ loadSearch, cleanErrors }, dispatch);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SchedulerPage));
