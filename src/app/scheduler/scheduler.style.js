import styled from "styled-components";
import { StructureLayout } from "../../commons/layout";
import { device } from "../../commons/devices";

export const Wrappers = {
    header: styled.div`
        /* background-color: #3489DA; */
        background-image: linear-gradient(180deg, #25dde0, #18d1d6 50%, #0ac1ff);
        padding: 70px 0;
        @media ${device.mobile}, ${device.miniTablet} {
            padding: 20px 0;
        }
    `,
    blackLayer: styled.div`
        background-color: rgba(0,0,0,0.25);
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        z-index: 1;
    `,
    body: styled(StructureLayout.Row)`
        height: 40vh;
        content: "";
        background-color: #f3f3f3;
    `,
    bottom: styled(StructureLayout.Row)`
        height: 10vh;
        content: "";
    `,
    bg: styled.div`
        height: 100%;
        position: absolute;
        width: 100%;
    `,
    container: styled(StructureLayout.Container)`
        z-index: 2;
    `,
    title: styled.h2`
        margin-bottom: 45px;
        color: #FFF;
    `
}