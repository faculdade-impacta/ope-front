import React, { Component } from "react";
import SearchInput from "../../../../commons/components/search-bar"
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from 'react-router-dom';
import { StructureLayout } from "../../../../commons/layout";
import { registerSearch } from "../../../../services/appServices";
import { Constants } from '../../../../config/constants';
import Autosuggest from 'react-autosuggest';
import { cleanErrors } from '../../../root/app.actions';

const getSuggestions = value => {
    const inputValue = value.trim().toLowerCase();
    const inputLength = inputValue.length;

    return inputLength === 0 ? [] : Constants.UF.filter(lang =>
        lang.nome.toLowerCase().slice(0, inputLength) === inputValue
    );
};

const getSuggestionValue = suggestion => suggestion.nome;

const renderSuggestion = suggestion => (
    <div>
        {suggestion.nome}
    </div>
);

class Search extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchParameters: {
            },
            value: "",
            suggestions: []
        };
        this.sendQuery = this.sendQuery.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.onSuggestionsFetchRequested = this.onSuggestionsFetchRequested.bind(this);
        this.onSuggestionsClearRequested = this.onSuggestionsClearRequested.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    componentDidMount() {
        this.props.cleanErrors();
    }

    sendQuery() {
        if (this.state.searchParameters.onLocation || this.state.searchParameters.searchTerm) {
            this.props.registerSearch(this.state.searchParameters);
            this.props.history.push(`/resultados&${this.state.searchParameters.searchTerm}&${this.state.searchParameters.onLocation ? this.state.searchParameters.onLocation : 'all'}`);
        }
    }

    handleInputChange(e) {
        this.setState({
            searchParameters: {
                ...this.state.searchParameters, [e.target.name]: e.target.value.toString().trim()
            }
        })
    }

    onChange = (event, { newValue }) => {
        this.setState({
            value: newValue,
            searchParameters: {
                onLocation: newValue,
                searchTerm: this.state.searchParameters.searchTerm
            }
        });
    };

    onSuggestionsFetchRequested = ({ value }) => {
        this.setState({
            suggestions: getSuggestions(value)
        });
    };

    onSuggestionsClearRequested = () => {
        this.setState({
            suggestions: []
        });
    };

    render() {
        const { value, suggestions } = this.state;

        const inputProps = {
            placeholder: 'Qual a Região?',
            value,
            onChange: this.onChange
        };

        return (
            <div className="wrap-content-form">
                <h1>Encontre <strong>profissionais</strong> perto de você!</h1>
                <form onSubmit={this.sendQuery}>
                    <SearchInput
                        required={true}
                        name="searchTerm"
                        placeholder="O que você procura?"
                        onChange={this.handleInputChange}
                        value={this.state.searchParameters.searchTerm}
                    />
                    <StructureLayout.Row className="location-box">
                        <StructureLayout.Col sm="12" md="7" lg="7">
                            <Autosuggest
                                suggestions={suggestions}
                                onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                                onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                                getSuggestionValue={getSuggestionValue}
                                renderSuggestion={renderSuggestion}
                                inputProps={inputProps}
                            />
                        </StructureLayout.Col>
                        <StructureLayout.Col sm="12" md="1" lg="1">
                        </StructureLayout.Col>
                        <StructureLayout.Col className="wrap-button" sm="12" md="4" lg="4">
                            <button className="btn-primary" type="submit">Buscar</button>
                        </StructureLayout.Col>
                    </StructureLayout.Row>
                </form>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    app: state.app,
})

const mapDispatchToProps = dispatch => bindActionCreators({ registerSearch, cleanErrors }, dispatch)

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Search));