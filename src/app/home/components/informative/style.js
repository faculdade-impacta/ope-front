import styled from "styled-components";
import Paper from '@material-ui/core/Paper';
import { device } from "../../../../commons/devices"

export const InformativeElements = {
    container: styled(Paper)`
        text-align: center;
        height: 40vh;
        display: flex;
        align-items: center;
        justify-content: center;
        z-index: 1;
        margin: 8px;
    `,
    message: styled.span`
        max-width: 85%;
    `,
}