import styled from "styled-components";
import DefaultButton from "../../commons/components/default-button";
import { StructureLayout } from "../../commons/layout";
import { device } from "../../commons/devices";
import { Container } from "reactstrap";

export const HomeElements = {
    AdvertiseButton: styled(DefaultButton)`
        background-color: #673AB7 !important;
    `,
    row: styled.div`
        text-align: center;
        height: 40vh;
        width: 100%;
        display: flex;
        margin-top: 50px;
        @media ${device.mobile}, ${device.miniTablet}, ${device.tablet} {
            flex-wrap: wrap;
        }
    `,
    Title: styled.span`
        font-size: 26px;
        font-weight: 500;
        margin-bottom: 32px;
    `,
    Container: styled(Container)`
        @media ${device.mobile}, ${device.miniTablet}, ${device.tablet} {
            padding: 0 !important;
        }
    `,
} 