import React, { Component } from "react";
import { withRouter } from 'react-router-dom';
import { Link } from "react-router-dom";
import ForgetPassword from "./containers/reset-pw";

class ForgotPassPage extends Component {
    
    render() {
        return (
            <section className="authentication-form reset-pass">
                <div>
                    <Link to="/"><img className="auth-logo" src="assets/images/app/marca-positivo.svg" alt="Cuidador Online"/></Link>
                    <ForgetPassword/>
                </div>
            </section>
        );
    }
} 
export default withRouter(ForgotPassPage);