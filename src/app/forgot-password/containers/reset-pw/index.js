import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { recoverPassword } from "../../../../services/userServices";
import { withRouter } from 'react-router-dom';
import { cleanErrors } from '../../../root/app.actions';

class ForgetPassword extends React.Component {

   constructor(props) {
      super(props);
      this.state = {
            accessValidation: false,
            requestLink: false
      };
      this.onSubmit = this.onSubmit.bind(this);
   }

   componentDidMount() {
      this.props.cleanErrors();
   }

   onSubmit(e) {
      e.preventDefault();
      if (e.target.mail.value) {
         recoverPassword(e.target.mail.value);
         this.setState({requestLink: true});
         setTimeout(()=> {this.props.history.push('/')}, 1000);
      } else {
         this.setState({accessValidation: true});
      }
      return null
  }

   render() {
        return (
            <div className="card">
               <div className="form-group mt-2">
                  <p className="mt-0 mb-3">Esqueceu sua senha? <span role="img" aria-label="Emoji: cuidador online (c.online) preocupado, porque você não está tendo acesso a plataforma para contratar um cuidador">😱</span> <br/>Sem problema! A gente te ajuda a recuperar. <span role="img" aria-label="Emoji: cuidador online (c.online) tranquilo, porque você agora conseguirá encontrar e buscar por um profissional">😉</span></p>
                  {
                     this.state.accessValidation ? 
                     <p className="text-center">Verifique o email informado</p> :
                     ''
                  }
                  <form className="theme-form" onSubmit={ this.onSubmit }>
                     {
                     !this.state.requestLink ? <div className="form-row">
                        <div className="col-12">
                           <input type="mail" name="mail" className="form-control digits mb-1" placeholder="Digite aqui o seu email" />
                        </div>
                        <div className="col-12 mt-4">
                           <button type="submit" className="btn btn-custom btn-block theme-color">Receber link de recuperação</button>
                        </div>
                     </div> : <div>
                        <p>Verifique seu email para configurar a nova senha.</p>
                     </div>
                     }
                  </form>
               <p className="text-center mt-0 mb-3">Não recebeu nosso email de recuperação? <a href="mailto:contato@cuidador.online?subject=Recuperar acesso a conta" className="text-danger">Clique aqui e entre em contato.</a></p>
               </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
   app: state.app,
   session: state.session
});

const mapDispatchToProps = dispatch => bindActionCreators({ cleanErrors }, dispatch)

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ForgetPassword));