import styled from "styled-components";
import { Grid, Button } from '@material-ui/core'
import DefaultButton from "../../../../commons/components/default-button";
import { device } from "../../../../commons/devices"



export const ServiceElements = {
    container: styled.div`
        margin: 40px 0px 25px 0px;
        max-width: 1500px;
        text-align: left;
        display: flex;
        flex-direction: column;
        justify-content: center;
    `,
    service: styled.div`
        display: flex;
        flex-direction: row;
        max-width: 1060px;        
        justify-content: space-between;
        min-height: 310px;
        @media ${device.lMobileWide} { 
            & {
                min-height: 250px;
            }
        }
    `,
    serviceText: styled.div`
        margin-right: 20px;
        margin-left: 20px;
        max-width: 60%;
    `,
    options: styled.div`
        margin: 5% 0px;
        max-width: 140px;
        display: flex;
        flex-direction: column;
        justify-content: space-evenly;
        width: -webkit-fill-available;
        @media ${device.mobile}, ${device.miniTablet}, ${device.tablet}, ${device.mobileWide} {
            display: flex;
            flex-direction: column;
            justify-content: center;
            max-width: 120px;
            width: -webkit-fill-available;
        }
    `,

    subTitle: styled.span`
        font-weight: 500;
        text-align: left;
    `,
    title: styled.h3`
        text-align: left;
    `,
    edit: styled.div`
        @media ${device.mobile}, ${device.miniTablet}, ${device.tablet} {
            margin: 10px;
        }
    `,
    delete: styled.div`
        @media ${device.mobile}, ${device.miniTablet}, ${device.tablet} {
            margin: 10px;
        }
    `,
    elementContainer: styled(Grid)`
        margin-bottom: 40px !important;
    `
}

export const CreateUpdateElements = {
    inputSizer: styled.div`
        width: 50%;
    `,
    flexDiv: styled.div`
        display: flex;
    `,
    titleContainer: styled.div`
        padding-right: 10px;
        width: 50%;
    `,
    createService: styled.div`
        text-align: left;
    `,
    btnContainer: styled.div`
        display: flex;
        flex-direction: column;
        justify-content: space-between;
        align-items: center;
        margin-top: 35px;
    `,
    salvar: styled.div`
        width: 100%;
        margin-bottom: 20px;
    `,
    cancelar: styled.div`
        width: 100%;
        margin-bottom: 10px;
    `,
}