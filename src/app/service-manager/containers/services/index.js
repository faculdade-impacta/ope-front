import React, { Component } from "react";
import { ServiceElements } from "./style";
import ServiceItem from "../item-service";
import { getListServices } from "../../service-manager.actions";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { cleanErrors } from '../../../root/app.actions';

class Services extends Component {

    // componentWillMount() {
        
    // }

    componentDidMount() {
        this.props.cleanErrors();
        this.props.getListServices();
    }

    render() {

        const { error, loading, user: { publishedServices } } = this.props.app;

        if (publishedServices.length === 0)
            return (
                <div className="row not-found-container">
                    <div className="not-found-message">
                        <p className="line">Você não tem nenhum serviço cadastro.</p>
                        <p className="line">Crie um agora mesmo! Clique em "Criar um Serviço" e preencha formulário </p>
                    </div>
                </div>
            )

        if (publishedServices)
            return (
                <ServiceElements.container>
                    {
                        publishedServices.map((element, index) => (
                            <ServiceItem
                                key={index}
                                service={element}
                                publishedServices={publishedServices}
                            />
                        ))
                    }
                </ServiceElements.container>
            )
        if (error) return <h2>Atenção! {error}</h2>
        if (loading) return <h2>Carregando</h2>
        return <div> 404 </div>
    }
}

const mapStateToProps = state => ({
    session: state.session,
    app: state.app
});

const mapDispatchToProps = dispatch => bindActionCreators({ getListServices, cleanErrors }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Services);