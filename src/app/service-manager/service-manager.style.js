import styled from "styled-components";
import { StructureLayout } from "../../commons/layout";
import DefaultButton from "../../commons/components/default-button";

export const Wrappers = {
    header: styled.div`
        height: 280px;
        max-height: 250px;
        background-image: linear-gradient(180deg,#25dde0,#18d1d6 50%,#0ac1ff);
        color: #673AB7;
        background-repeat: no-repeat;
        background-size: 100%;
        background-position-y: center;
        display: flex;
        flex-direction: column;
        justify-content: space-around;
        padding: 60px 0;
        margin-bottom: 0px;
        z-index: 0;
    `,
    blackLayer: styled.div`
        background-color: rgba(0,0,0,0.25);
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        z-index: 1;
    `,
    body: styled(StructureLayout.Row)`
        height: 40vh;
        content: "";
        background-color: #f3f3f3;
    `,
    bottom: styled(StructureLayout.Row)`
        height: 10vh;
        content: "";
    `,
    bg: styled.div`
        height: 100%;
        position: absolute;
        width: 100%;
    `,
    container: styled(StructureLayout.Container)`
        text-align: center;
        z-index: 2;
        height: 120px;
        position: relative;
        background-color: #673AB7;
    `,
    container: styled(StructureLayout.Container)`
    text-align: center;
    z-index: 2;
    height: 120px;
    position: relative;
    background-color: #673AB7;
    `,
    myServices: styled(StructureLayout.Container)`
        z-index: 2;
        height: 120px;
        align-items: center;
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        max-width: 950px;
        margin: 0 auto;
        margin-top: 90px;
    `,
    create: styled(DefaultButton)`
        color: #FFF !important;
        height: 40px;
        width: 180px;
    `,
    title: styled.h2`
        margin-bottom: 0px;
        color: #FFF;
        text-align: left;
    `
}

export const CreateUpdateElements = {
    inputSizer: styled.div`
        width: 50%;
    `,
    flexDiv: styled.div`
        display: flex;
    `,
    titleContainer: styled.div`
        padding-right: 10px;
        width: 50%;
    `,
    createService: styled.div`
        text-align: left;
    `,
    btnContainer: styled.div`
        display: flex;
        flex-direction: column;
        justify-content: space-between;
        align-items: center;
        margin-top: 50px;
    `,
    salvar: styled.div`
        width: 100%;
        margin-bottom: 20px;
    `,
    cancelar: styled.div`
        width: 100%;
        margin-bottom: 10px;
    `,
}