import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { Login } from "../../signin.actions";
import { facebookLogin, googleLogin } from '../../../signin/signin.actions';

import { LoginFormsElements } from "./style";
import { DotLoader } from 'react-spinners';
import { Link } from "react-router-dom";
import { cleanErrors } from '../../../root/app.actions';

class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            type: 'password',
            accessValidation: false,
            termsChecked: true
        };
        this.showHide = this.showHide.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    componentDidMount() {
        this.props.cleanErrors();
    }

    onSubmit(e) {

        e.preventDefault();

        let credentials = {
            username: e.target.username.value,
            password: e.target.password.value
        }

        if (credentials.username && credentials.password) {
            this.props.Login(credentials);
            this.setState({ accessValidation: false });
            return null
        } else {
            this.setState({ accessValidation: true });
        }
        return null
    }

    showHide(e) {
        e.preventDefault();
        this.setState({
            type: this.state.type === 'input' ? 'password' : 'input'
        })
    }

    render() {
        const { error, loading } = this.props.app;
        const { authenticated } = this.props.session;
        const accessValidation = this.state.accessValidation;
        if (authenticated) {
            this.props.history.goBack();
        }
        if (loading) {
            return <div>
                {
                    !accessValidation && !accessValidation &&
                    <p className="text-center">Bem vindo Cuidador Online, Por favor entre com as credenciais da sua conta.</p>
                }
                {
                    accessValidation &&
                    <p className="text-center">Verifique os dados de acesso</p>
                }
                {
                    error && !accessValidation &&
                    <p className="text-center">{error}</p>
                }
                <div className="card">
                    <LoginFormsElements.wrapLoader>
                        <DotLoader
                            sizeUnit={"px"}
                            size={50}
                            color={'#25dde0'} />
                    </LoginFormsElements.wrapLoader>
                </div>
            </div>
        }
        return <div>
            {
                !accessValidation && !accessValidation &&
                <p className="text-center">Bem vindo Cuidador Online! Escolha uma forma de acesso a plataforma</p>
            }


            {
                accessValidation &&
                <p className="text-center error">Verifique os dados de acesso</p>
            }
            {
                error && !accessValidation &&
                <p className="text-center error">{error}</p>
            }
            <div className="card">
                <h6 className="text-center mt-0 mb-3">Acessar com:</h6>
                <div className="form-button text-center social-btns">
                    <button type="button" className="btn btn-custom fb" onClick={this.props.facebookLogin}>Facebook</button>
                    <button type="button" className="btn btn-custom ggl" onClick={this.props.googleLogin}>Google</button>
                </div>
                <hr/>
                <div className="form-button text-center">
                    <div className="terms-label">
                        <input type="checkbox" checked={this.state.termsChecked} onChange={this.handleTermsConditions} id="agree"/>
                        <label htmlFor="agree">Declaro que concordo com os </label><strong data-toggle="modal" data-target="#terms-conditions"> Termos de Uso</strong>.
                    </div>
                </div>
                <div className="modal fade" id="terms-conditions" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div className="modal-dialog modal-dialog-centered" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="exampleModalLabel">Termos de Uso do Cuidador Online</h5>
                                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div className="modal-body">
                                        <h5>Título Lorem Teste</h5>
                                        <p>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus luctus justo, in ultrices risus dictum eu. Integer id urna a risus pulvinar finibus eget in ipsum. Phasellus luctus purus enim, ac accumsan tellus accumsan aliquam. Nunc sed condimentum ligula. Sed purus neque, volutpat sed dignissim lobortis, scelerisque sit amet eros. Duis sollicitudin suscipit velit, eu condimentum enim imperdiet porta. Cras sit amet metus at lectus faucibus elementum. Vestibulum non porttitor neque, ut commodo nisi. In at nulla egestas, scelerisque nisl et, molestie nunc.
                                        </p>
                                        <h5>Título Lorem Teste</h5>
                                        <p>
                                            Nunc sodales id arcu at bibendum. Suspendisse semper diam vestibulum commodo feugiat. Donec molestie purus odio, ac elementum tortor interdum sit amet. Integer rhoncus metus arcu, vel consectetur nisi varius in. Vestibulum nec enim eu tellus lacinia porta. In justo nisi, semper a nibh id, mattis porta justo. Mauris eleifend ligula ut diam tempor, at feugiat diam interdum. Curabitur dictum felis ac nibh dictum auctor. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Integer ac mauris libero. Duis eu quam nec diam porttitor placerat. Nullam scelerisque urna leo, sed aliquet justo fermentum sed. Suspendisse pellentesque augue elementum libero elementum, sit amet tempor mi rhoncus.
                                        </p>
                                        <h5>Título Lorem Teste</h5>
                                        <p>
                                            Ut dapibus, urna et egestas suscipit, ex turpis sodales metus, non hendrerit tortor mi eget nisi. Suspendisse pulvinar, lacus sit amet vehicula condimentum, eros erat ultricies dui, tempor egestas justo dui non ante. Sed efficitur lacus est. Sed vulputate ac turpis in sollicitudin. Etiam lacinia quis orci sit amet vehicula. Pellentesque laoreet, odio non maximus ullamcorper, libero risus pulvinar sem, vitae commodo diam neque eget nulla. Pellentesque sed lorem sed eros consequat facilisis molestie sed quam. Morbi tincidunt nec libero sit amet placerat.
                                        </p>
                                        <h5>Título Lorem Teste</h5>
                                        <p>
                                            Aliquam quis arcu sit amet sem sodales condimentum nec sed neque. Maecenas tincidunt nunc ut nunc tincidunt semper. Aliquam quis mollis lacus. Curabitur sit amet sapien a purus dictum laoreet. Aenean ut posuere eros, sit amet consectetur eros. Cras id dolor in leo tincidunt mattis ut vel velit. Etiam non sapien eu turpis congue luctus sed molestie magna. Quisque hendrerit, arcu quis rutrum vehicula, nibh lectus congue nibh, ac pellentesque magna lectus commodo nisi. Praesent consequat id nunc ut venenatis. Sed varius malesuada fringilla.
                                        </p>
                                        <h5>Título Lorem Teste</h5>
                                        <p>
                                            Etiam scelerisque diam eget viverra pretium. Quisque eget cursus turpis. Maecenas sed accumsan sapien, ut tincidunt tellus. Aenean ut leo sit amet risus blandit maximus ac et eros. Vestibulum pharetra, felis in volutpat ornare, nulla leo aliquam sapien, sollicitudin condimentum velit turpis a enim. Aliquam volutpat tellus a velit aliquet mollis. Proin porttitor ante quis libero auctor ultricies. Etiam non felis vitae eros ultrices vulputate. Fusce eros tortor, porta ac consequat egestas, ultricies id risus. Nam sagittis aliquam diam. Donec laoreet orci quis diam scelerisque, in finibus tortor lobortis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla nisi odio, fringilla et diam nec, pellentesque hendrerit dui. Donec pretium tellus vitae enim vestibulum, vel efficitur enim hendrerit. Suspendisse ultricies odio vitae suscipit egestas.
                                        </p>
                                    </div>
                                    <div className="modal-footer">
                                        {this.state.termsChecked}
                                        <input type="checkbox" checked={this.state.termsChecked} onChange={this.handleTermsConditions} />
                                        <span>Declaro que concordo com os termos de uso.</span>
                                        <button data-dismiss="modal" className="btn btn-custom">Fechar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
            </div>
        </div>
    };

}

const mapStateToProps = state => ({
    app: state.app,
    session: state.session
});

const mapDispatchToProps = dispatch => bindActionCreators({ Login, facebookLogin, googleLogin, cleanErrors }, dispatch)

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(LoginForm));