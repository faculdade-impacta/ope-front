import React, { Component } from "react";
import { withRouter } from 'react-router-dom';
import { Link } from "react-router-dom";
import LoginForm from "./containers/login-form";

class SigninPage extends Component {
    
    render() {
        return (
            <section className="authentication-form">
                <div>
                    <Link to="/"><img className="auth-logo" src="assets/images/app/marca-positivo.svg" alt="Cuidador Online"/></Link>
                    <LoginForm/>
                </div>
            </section>
        );
    }
} 
export default withRouter(SigninPage);