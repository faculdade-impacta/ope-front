import { userSignIn, facebookSignIn, googleSignIn } from "../../services/userServices";
import AppActions from '../root/app.actions';

export const facebookLogin = () => (dispatch) => {
    dispatch({
        type: AppActions.APP_LOADING,
        payload: null
    });
    facebookSignIn().then(
        res => dispatch({
            type: AppActions.MAKE_LOGIN,
            payload: res
        })
    ).catch(
        err => dispatch({
            type: AppActions.APP_ERROR,
            payload: err
        })
    )
}

export const googleLogin = () => (dispatch) => {
    dispatch({
        type: AppActions.APP_LOADING,
        payload: null
    });
    googleSignIn().then(
        res => dispatch({
            type: AppActions.MAKE_LOGIN,
            payload: res
        })
    ).catch(
        err => dispatch({
            type: AppActions.APP_ERROR,
            payload: err
        })
    )
}

export const Login = (credentials) => (dispatch) => {
    dispatch({
        type: AppActions.APP_LOADING,
        payload: null
    });
    userSignIn(credentials)
        .then((res) => {
            dispatch({
                type: AppActions.MAKE_LOGIN,
                payload: res
            });
        }).catch((err) => {
            dispatch({
                type: AppActions.APP_ERROR,
                payload: err.message
            });
        });  
}



