import React, { Component } from "react"
import { StructureLayout } from "../../commons/layout";
import { Wrappers } from "./signup.style";
import RegisterForm from "./containers/register-form";
import MenuNavbar from "../../commons/containers/navbar";

class SignUp extends Component {
    render() {
        return (
            <div className="app-signup">
                <RegisterForm />
            </div>
        )
    }
}

export default SignUp;
