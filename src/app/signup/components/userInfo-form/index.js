import React, { Component } from "react";
import { InputElements } from "./style";
import { StructureLayout } from "../../../../commons/layout";
import { TextField, Radio, RadioGroup, FormControlLabel, FormLabel } from '@material-ui/core';

class UserInfoForm extends Component {

    constructor (props) {
        super(props);

        this.state = { 
            cSelected: [],
            userName: '',
            userSurname: '',
            gender: 'feminino',
         };

        this.onRadioBtnClick = this.onRadioBtnClick.bind(this);
    }

    onRadioBtnClick(rSelected) {
        this.setState({ rSelected });
    }

    handleChange = event => {
        this.setState({ [event.target.name]: event.target.value });
    };

    render() {
        return (
            <StructureLayout.Grid container spacing={24}>
                <StructureLayout.Grid item xs={6}>
                    <TextField
                        name="userName" 
                        label="Nome"
                        onChange={ (e) => this.props.onChange(e, "name") }
                        margin="normal"
                        variant="filled"
                        fullWidth
                    />
                </StructureLayout.Grid>
                <StructureLayout.Grid item xs={6}>
                    <TextField
                        name="userSurname"
                        label="Sobrenome"
                        onChange={ (e) => this.props.onChange(e, "surname") }
                        margin="normal"
                        variant="filled"
                        fullWidth
                    />
                </StructureLayout.Grid>
                <StructureLayout.Grid item xs={6}>
                    <FormLabel component="legend">Genêro</FormLabel>
                    <Radio
                        checked={this.state.gender === 'feminino'}
                        onChange={this.handleChange}
                        value="feminino"
                        name="gender"
                        aria-label="Gender"
                    /> Feminino
                    <Radio
                        checked={this.state.gender === 'masculino'}
                        onChange={this.handleChange}
                        value="masculino"
                        name="gender"
                        aria-label="Gender"
                    /> Masculino
                </StructureLayout.Grid>
                <StructureLayout.Grid item xs={6}>
                <FormLabel component="legend">Data Nascimento:</FormLabel>
                    <TextField
                        id="date"
                        type="date"
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />
                </StructureLayout.Grid>
            </StructureLayout.Grid>
        );
    }
} 


export default UserInfoForm;