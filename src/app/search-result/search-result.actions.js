import http from "../../services/httpServices";
import AppActions from "../root/app.actions"
import { getCurrentUser } from "../../services/sessionServices";

export const loadSearch = (onLocation, searchTerm) => (dispatch) => {
    if (!onLocation && !searchTerm) {
        dispatch({
            type: AppActions.APP_ERROR,
            payload: 'É necessario inserir os parametros para a pesquisa'
        });
    };
    dispatch({
        type: AppActions.APP_LOADING,
        payload: null
    });
    let endpoint = `/search?q=${searchTerm}&l=${onLocation}`;
    if (!getCurrentUser()) {
        endpoint = `/search/public?q=${searchTerm}&l=${onLocation}`;
    }
    http.get(endpoint)
        .then((response) => {
            dispatch({
                type: AppActions.SEARCH_SUCCESS,
                payload: response.data
            });
        }).catch((error) => {
            dispatch({
                type: AppActions.APP_ERROR,
                payload: error
            })
        })
}