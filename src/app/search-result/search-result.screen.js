import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from 'react-router-dom';
import { loadSearch } from './search-result.actions';
import MenuNavbar from "../../commons/containers/navbar";
import SearchBar from "./containers/search-bar";
import ResultList from "./containers/result-list";
import { Wrappers } from "./search-result.style";
import { cleanErrors } from '../root/app.actions';

class SearchResultPage extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        this.props.cleanErrors();
        const {query, region} = this.props.match.params;
        this.props.loadSearch(region, query);
    }

    render() {
        return (
            <div className="app-search-result">
                <MenuNavbar />
                <Wrappers.header>
                    <Wrappers.container>
                        <SearchBar message="Encontrar" />
                    </Wrappers.container>
                </Wrappers.header>
                <ResultList />
            </div>
        )
    }
}

const mapStateToProps = state => ({
    app: state.app,
})
const mapDispatchToProps = dispatch => bindActionCreators({ loadSearch, cleanErrors }, dispatch);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SearchResultPage));
