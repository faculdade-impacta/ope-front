import styled from "styled-components";
import DefaultButton from "../../../../commons/components/default-button";
import { StructureLayout } from "../../../../commons/layout";
import { device } from "../../../../commons/devices";

export const SearchElements = {
    wrap: styled.div`
        height: 100%;
        padding: 0;
        margin: 0;
        display: -webkit-box;
        display: -moz-box;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        align-items: center;
        justify-content: center;
    `,
    wrapCol: styled.div`
        margin: 0px 15px 0px 15px;
    `,
    Button: styled(DefaultButton)`
        @media ${device.mobile} {
            margin-top: 25px !important;
        }
    `,
    wrapSearch: styled.div`
        display: flex;
    `,
    regionDiv: styled.div`
        float: left !important;
        position: relative !important;
        margin-top: 10px !important;
    `,
}