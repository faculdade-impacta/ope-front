import styled from "styled-components";
import { StructureLayout } from "../../commons/layout";

export const Wrappers = {
    header: styled(StructureLayout.Row)`
        height: 50vh;
        background-color: #e4e4e4;
        display: flex;
        flex-direction: column;
        justify-content: space-around;
        padding: 60px 0;
    `,
    body: styled(StructureLayout.Row)`
        height: 40vh;
        content: "";
        background-color: #f3f3f3;
    `,
    bottom: styled(StructureLayout.Row)`
        height: 10vh;
        content: "";
    `,
    bg: styled.div`
        height: 100%;
        position: absolute;
        width: 100%;
    `,
    container: styled(StructureLayout.Container)`
        position: absolute;
        top: 55px;
    `
}