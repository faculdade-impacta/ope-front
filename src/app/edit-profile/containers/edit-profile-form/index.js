import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from 'react-router-dom';
import { editUser, changePass } from '../../../signup/signup.actions';
import { cleanErrors } from '../../../root/app.actions';
import { RegisterElements } from "./style";
import { StructureLayout } from "../../../../commons/layout";
import InputMask from 'react-input-mask';

import { DotLoader } from 'react-spinners';

class EditProfileForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            profilePicture: ''
        };
        this.onSubmit = this.onSubmit.bind(this);
        this.handleFileUpload = this.handleFileUpload.bind(this);
    }

    componentDidMount() {
        this.props.cleanErrors();
    }

    onSubmit(event) {
        event.preventDefault();

        console.log("Form: ", event.target);
        
    }

    handleFileUpload(event) {
        this.setState({profilePicture: event.target.files[0]});
    }

    render() {
        const { loading, error } = this.props.app;
        const user = this.props.user;
        let userPicture = this.state.profilePicture !== '' ? this.state.profilePicture : user.profile_picture;

        console.log(userPicture);

        if (loading) {
            return (
                <RegisterElements.wrapLoader>
                    <DotLoader
                        sizeUnit={"px"}
                        size={50}
                        color={'#25dde0'} />
                </RegisterElements.wrapLoader>
            )
        }
        let parsed = {}
        parsed.professional = window.location.search ? window.location.search.split('?')[1].split('=')[0] : undefined;

        return (
            <RegisterElements.form onSubmit={this.onSubmit}>
                {
                    error &&
                    <RegisterElements.error>{error.message}</RegisterElements.error>
                }
                {
                    parsed.professional ? <h2 className="profileTitle">Completar Cadastro</h2> : <h2 className="profileTitle">Meu Perfil</h2>
                }
                <StructureLayout.Grid container spacing={24}>
                    <StructureLayout.Grid className="editAvatar" item xs={12} md={3}>
                        <label htmlFor='select-avatar'>
                        <img htmlFor='select-avatar' src={userPicture} alt="profile picture"/>
                        <i className="fa fa-camera"/>
                        </label>
                        <input
                            type="file"
                            name="file"
                            id="select-avatar"
                            accept="image/*"
                            onChange={this.handleFileUpload}
                            className="btn btn-upload-img btn-primary"
                        />
                    </StructureLayout.Grid>
                    <StructureLayout.Grid item xs={12} md={9}>
                        <RegisterElements.formLabel component="legend">Nome:</RegisterElements.formLabel>
                        <input
                            name="name"
                            required="required"
                            type="text"
                            maxLength={50}
                            ref={this.input}
                            defaultValue={user.full_name}
                        />
                    </StructureLayout.Grid>
                    <StructureLayout.Grid item xs={12} md={6}>
                    <RegisterElements.formLabel component="legend">Email:</RegisterElements.formLabel>
                        <input
                            name="email"
                            label="Email"
                            type="email"
                            maxLength={50}
                            ref={this.input}
                            defaultValue={user.email}
                        />
                    </StructureLayout.Grid>
                    {/* <StructureLayout.Grid item xs={12} md={4}>
                        <RegisterElements.formLabel component="legend">Genêro:</RegisterElements.formLabel>
                        <div className="genderInput">
                            <label><input type="radio" value="M" name="gender"/>Masculino</label>
                        </div>
                        <div className="genderInput">
                            <label><input type="radio" value="F" name="gender"/>Feminino</label>
                        </div>
                    </StructureLayout.Grid> */}
                    <StructureLayout.Grid item xs={12} md={6}>
                        <RegisterElements.formLabel component="legend">Telefone:</RegisterElements.formLabel>
                        <input
                            mask="+55 (99) 99999 - 9999"
                            ref={this.input}
                            defaultValue={user.phone || ''}
                            name="phone_number"
                            id="phone_number"
                        />
                    </StructureLayout.Grid>
                    <StructureLayout.Grid item xs={12} md={6}>
                    </StructureLayout.Grid>
                    <StructureLayout.Grid item xs={10}>
                        <RegisterElements.formLabel component="legend">Cidade:</RegisterElements.formLabel>
                        <input
                            name="userCity"
                            ref={this.input}
                            defaultValue={user.user_location ? user.user_location.location.city : ''}
                            maxLength={50}
                        />
                    </StructureLayout.Grid>
                    <StructureLayout.Grid item xs={2}>
                        <RegisterElements.formLabel component="legend">Estado:</RegisterElements.formLabel>
                        <input
                            name="userState"
                            ref={this.input}
                            defaultValue={user.user_location ? user.user_location.location.uf : ''}
                            maxLength={2}
                        />
                    </StructureLayout.Grid>
                    
                </StructureLayout.Grid>
                <button type="submit" className="btn send-form-btn btn-primary">Atualizar</button>
            </RegisterElements.form>
        );
    }
}

const mapStateToProps = state => ({
    register: state.register,
    session: state.session,
    user: state.session.user,
    app: state.app
});

const mapDispatchToProps = dispatch => bindActionCreators({ editUser, changePass, cleanErrors }, dispatch);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EditProfileForm));