import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from 'react-router-dom';
import { loadSearch } from './professional-form.actions';
import MenuNavbar from "../../commons/containers/navbar";
import { Wrappers } from "./professional-form.style";
import { cleanErrors } from '../root/app.actions';
import { StructureLayout } from "../../commons/layout";
import SweetAlert from "react-bootstrap-sweetalert";
class ProfessionalFormPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            alert: false
        };
        this.completeRegistration = this.completeRegistration.bind(this);
    }

    componentDidMount() {
        this.props.cleanErrors();
        const { query, region } = this.props.match.params;
        this.props.loadSearch(region, query);
    }

    completeRegistration() {
        this.setState({ alert: false })
        window.location = "/"
    }

    render() {
        return (
            <div className="app-professional-form">
                <MenuNavbar />
                <Wrappers.header>
                </Wrappers.header>
                <StructureLayout.Container className="form-container">
                    <StructureLayout.Row >
                        <StructureLayout.Col md="12" lg="12" className="wrap-form">
                            <iframe name="hidden_iframe" id="hidden_iframe" style={{ display: "none" }}></iframe>
                            <form action="https://docs.google.com/forms/d/e/1FAIpQLSdKuFwBFZe1fhBc2aI3Hz0ns5VOCk37ozSGNF3jxa2l0YeSAg/formResponse"
                                target="hidden_iframe"
                                id="bootstrapForm"
                                method="POST">
                                <fieldset>
                                    <h2>Seja um cuidador.</h2>
                                    <div>
                                        <p>A plataforma Cuidador Online tem como objetivo a democratização do acesso ao cuidado. </p>
                                        <p>Você tem experiência, contato com a área e gostaria de se juntar a plataforma para receber ofertas de trabalho? </p>
                                        <p>Simples! Preencha o formulário abaixo e entraremos em contato 🙂</p>
                                        <p>Estamos montando o time de cuidadores online, seja um dos primeiros a se juntar a comunidade</p>
                                    </div>
                                </fieldset>

                                <fieldset>
                                    <legend for="211409609">Email</legend>
                                    <div class="form-group">
                                        <input id="21182776" type="text" name="entry.21182776" class="form-control" required />
                                    </div>
                                </fieldset>

                                <fieldset>
                                    <legend for="1050223662">Nome completo</legend>
                                    <div class="form-group">
                                        <input id="1650096272" type="text" name="entry.1650096272" class="form-control" required />
                                    </div>
                                </fieldset>


                                <fieldset>
                                    <legend for="1120746642">Número de celular</legend>
                                    <div class="form-group">
                                        <input id="1743374944" type="text" name="entry.1743374944" class="form-control" required />
                                    </div>
                                </fieldset>


                                <fieldset>
                                    <legend for="2033243489">CEP</legend>
                                    <div class="form-group">
                                        <input id="1612563382" type="text" name="entry.1612563382" class="form-control" required />
                                    </div>
                                </fieldset>


                                <fieldset>
                                    <legend for="2066600204">Sexo</legend>
                                    <div class="form-group">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="entry.1217998698" value="Masculino" required />
                                                Masculino
                </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="entry.1217998698" value="Feminino" required />
                                                Feminino
                </label>
                                        </div>
                                    </div>
                                </fieldset>

                                <fieldset>
                                    <legend for="775888569">Marque as opções que se adequam a sua formação</legend>
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="entry.1886862369" value="Possui curso técnico." />
                                                Possui curso técnico.
                </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="entry.1886862369" value="Possui graduação" />
                                                Possui graduação
                </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="entry.1886862369" value="Certificações na área" />
                                                Certificações na área
                </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="entry.1886862369" value="Tenho experiência comprovada" />
                                                Tenho experiência comprovada
                </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="entry.1886862369" value="Nenhuma das opções" />
                                                Nenhuma das opções
                </label>
                                        </div>
                                    </div>
                                </fieldset>

                                <fieldset>
                                    <legend for="1708599103">Quais tipos de serviços você oferece?</legend>
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="entry.302828427" value="Companhia" />
                                                Companhia
                </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="entry.302828427" value="Acompanhamento para consulta" />
                                                Acompanhamento para consulta
                </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="entry.302828427" value="Acompanhamento em atividades rotineiras (ex: mercado)" />
                                                Acompanhamento em atividades rotineiras (ex: mercado)
                </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="entry.302828427" value="Administração de medicação" />
                                                Administração de medicação
                </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="entry.302828427" value="Atividades domésticas" />
                                                Atividades domésticas
                </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="entry.302828427" value="Banho" />
                                                Banho
                </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="entry.302828427" value="Refeição" />
                                                Refeição
                </label>
                                        </div>
                                    </div>
                                </fieldset>


                                <fieldset>
                                    <legend for="1381671460">Fale um pouco de sua experiência como cuidador.</legend>
                                    <div class="form-group">
                                        <textarea id="1136279785" name="entry.1136279785" class="form-control" ></textarea>
                                    </div>
                                </fieldset>

                                <fieldset>
                                    <legend for="1202846374">Tem familiaridade com aplicativos de transporte. Ex: Uber, 99, Cabify?</legend>
                                    <div class="form-group">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="entry.543129733" value="Sim" required />
                                                Sim
                </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="entry.543129733" value="Não" required />
                                                Não
                </label>
                                        </div>
                                    </div>
                                </fieldset>


                                <fieldset>
                                    <legend for="1998197025">Gostaria de participar do nosso grupo no WhatsApp?</legend>
                                    <div class="form-group">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="entry.1199518210" value="Sim" required />
                                                Sim
                </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="entry.1199518210" value="Não" required />
                                                Não
                </label>
                                        </div>
                                    </div>
                                </fieldset>

                                <input type="hidden" name="fvv" value="1" />
                                <input type="hidden" name="fbzx" value="-2856733646185208695" />
                                <input type="hidden" name="pageHistory" value="0" />

                                <input class="btn btn-primary" type="submit" value="Enviar" onClick={() => this.setState({ alert: true })} />
                            </form>
                        </StructureLayout.Col>
                    </StructureLayout.Row>
                </StructureLayout.Container>
                <SweetAlert success
                    title="Pronto!"
                    show={this.state.alert}
                    closeOnClickOutside={true}
                    onConfirm={this.completeRegistration}>
                    Cadastro realizado com sucesso.<br />
                </SweetAlert>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    app: state.app,
})
const mapDispatchToProps = dispatch => bindActionCreators({ loadSearch, cleanErrors }, dispatch);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ProfessionalFormPage));
