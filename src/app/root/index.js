import React from 'react';
import PropTypes from 'prop-types';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';

import PrivateRoute from './PrivateRoute';
import SignUp from '../signup/signup.screen';
import EditProfile from '../edit-profile/edit-profile.screen';
import SigninPage from '../signin/signin.screen';
import SearchResult from '../search-result/search-result.screen';
import Scheduler from '../scheduler/scheduler.screen';
import ProfessionalForm from '../professional-form/professional-form.screen';
import Services from '../service-manager/service-manager.screen';
import HomePage from '../home/home.screen.js';
import TermsPage from '../terms/terms.screen.js';
import ForgotPassPage from '../forgot-password/forgot-password.screen';

const App = ({ authenticated, checked }) => (
  <Router>
    {checked &&
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route exact path="/cadastro" component={SignUp} />
        <Route exact path="/login" component={SigninPage} />
        <Route exact path="/termos-de-uso" component={TermsPage} />
        {/* <Route exact path="/resultados&:query&:region" component={SearchResult} /> */}
        <Route exact path="/agendar-cuidador" component={Scheduler} />
        <Route exact path="/cadastro-cuidador" component={ProfessionalForm} />
        {/* <Route exact path="/recuperar-senha" component={ForgotPassPage} /> */}
        {/* <PrivateRoute exact path="/servicos" component={Services} authenticated={authenticated} /> */}
        {/* <PrivateRoute exact path="/editar-perfil" component={EditProfile} authenticated={authenticated} /> */}
      </Switch>
    }
  </Router>
)

const { bool } = PropTypes;

App.propTypes = {
  authenticated: bool.isRequired,
  checked: bool.isRequired
};

const mapState = ({ session }) => ({
  checked: session.checked,
  authenticated: session.authenticated
});

export default connect(mapState)(App);
