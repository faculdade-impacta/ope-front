import AppActions from './app.actions';

const app = {
    loading: false,
    error: false,
    user: {
        publishedServices: []
    },
    searchParameters: {},
    searchResult: [],
}

export const appReducer = (cState = app, action) => {
    switch (action.type) {
        default:
            return cState
        case AppActions.APP_LOADING:
            return { ...cState, loading: true, error: false }
        case AppActions.APP_ERROR:
            return { ...cState, loading: false, error: action.payload }
            
        case AppActions.MAKE_SEARCH:
            return { ...cState, loading: false, error: false, searchParameters: action.payload }
        case AppActions.SEARCH_SUCCESS:
            return { ...cState, loading: false, error: false, searchResult: action.payload }
        case AppActions.UPDATE_PUBLISHED_SERVICESS:
            return { ...cState, loading: false, error: false, user: { publishedServices: action.payload } }
        case AppActions.MAKE_LOGIN:
            return { ...cState, loading: false, error: false }
        case AppActions.CLEAN_ERRORS:
            return { ...cState, loading: false, error: false }
    }
}