const AppActions = {
    APP_LOADING: "APP_LOADING",
    APP_ERROR: "APP_ERROR",
    MAKE_SEARCH: "MAKE_SEARCH",
    SEARCH_SUCCESS: "SEARCH_SUCCESS",
    UPDATE_PUBLISHED_SERVICESS: "UPDATE_PUBLISHED_SERVICESS",
    MAKE_LOGIN: "MAKE_LOGIN",
    CLEAN_ERRORS: "CLEAN_ERRORS"
}

export const cleanErrors = () => (dispatch) => {
    dispatch({
        type: AppActions.CLEAN_ERRORS,
        payload: null
    })
}

export default AppActions;