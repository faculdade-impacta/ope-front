
$(document).ready(function () {

    $('#professional-form').submit(function (event) {
        event.preventDefault()
        var extraData = {}
        {
            /* Parsing input date id=37799665 */
            var dateField = $("#37799665_date").val()
            var timeField = $("#37799665_time").val()
            let d = new Date(dateField)
            if (!isNaN(d.getTime())) {
                extraData["entry.37799665_year"] = d.getFullYear()
                extraData["entry.37799665_month"] = d.getMonth() + 1
                extraData["entry.37799665_day"] = d.getUTCDate()
            }
            if (timeField && timeField.split(':').length >= 2) {
                let values = timeField.split(':')
                extraData["entry.37799665_hour"] = values[0]
                extraData["entry.37799665_minute"] = values[1]
            }
        }
        $('#professional-form').ajaxSubmit({
            data: extraData,
            dataType: 'jsonp',  // This won't really work. It's just to use a GET instead of a POST to allow cookies from different domain.
            error: function () {
                // Submit of form should be successful but JSONP callback will fail because Google Forms
                // does not support it, so this is handled as a failure.
                alert('Form Submitted. Thanks.')
                // You can also redirect the user to a custom thank-you page:
                // window.location = 'http://www.mydomain.com/thankyoupage.html'
            }
        })
    })

    $('#schedule-form').submit(function (event) {
        event.preventDefault()
        var extraData = {}
        {
            /* Parsing input date id=160297191 */
            var dateField = $("#160297191_date").val()
            var timeField = $("#160297191_time").val()
            let d = new Date(dateField)
            if (!isNaN(d.getTime())) {
                extraData["entry.160297191_year"] = d.getFullYear()
                extraData["entry.160297191_month"] = d.getMonth() + 1
                extraData["entry.160297191_day"] = d.getUTCDate()
            }
            if (timeField && timeField.split(':').length >= 2) {
                let values = timeField.split(':')
                extraData["entry.160297191_hour"] = values[0]
                extraData["entry.160297191_minute"] = values[1]
            }
        }
        $('#schedule-form').ajaxSubmit({
            data: extraData,
            dataType: 'jsonp',  // This won't really work. It's just to use a GET instead of a POST to allow cookies from different domain.
            error: function () {
                // Submit of form should be successful but JSONP callback will fail because Google Forms
                // does not support it, so this is handled as a failure.
                alert('Form Submitted. Thanks.')
                // You can also redirect the user to a custom thank-you page:
                // window.location = 'http://www.mydomain.com/thankyoupage.html'
            }
        })
    })
    /*----------------------------------------
     passward show hide
     ----------------------------------------*/
    $('.show-hide').show();
    $('.show-hide span').addClass('show');

    $('.show-hide span').click(function () {
        if ($(this).hasClass('show')) {
            $('input[name="login[password]"]').attr('type', 'text');
            $(this).removeClass('show');
        } else {
            $('input[name="login[password]"]').attr('type', 'password');
            $(this).addClass('show');
        }
    });
    $('form button[type="submit"]').on('click', function () {
        $('.show-hide span').text('Show').addClass('show');
        $('.show-hide').parent().find('input[name="login[password]"]').attr('type', 'password');
    });


    /*----------------------------------------
     Pre Loader
     ----------------------------------------*/
    $(window).on('load', function () {
        $('.loader-wrapper').fadeOut('slow');
        $('.loader-wrapper').remove('slow');
    });

    /*----------------------------------------
    Dark Header
     ----------------------------------------*/
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        if (scroll >= 25) {
            $(".navbar").addClass("darkHeader");
        } else {
            $(".navbar").removeClass("darkHeader");
        }
    });

    /*----------------------------------------
    mobile menu nav click hide collapse
    ----------------------------------------*/
    var mobile_menu = $(window).width();
    if (mobile_menu < 991) {
        $("nav a.nav-link").on('click', function (event) {
            if (!$(this).hasClass('dropdown-toggle')) {
                $(".navbar-collapse").collapse('hide');
            }
        });
    }

    $("nav button.navbar-toggler").on('click', function (event) {
        if ($("#toggler").attr('aria-expanded') == "true") {
            setTimeout(function () { $("#search-form").css('opacity', ''); }, 500);
        }
        else if ($("#toggler").attr('aria-expanded') == "false") {
            $("#search-form").css('opacity', '.0');
        }
    });

    /*----------------------------------------
    home removeclass section
   ----------------------------------------*/
    var slider_caption = $(window).width();
    if (slider_caption >= 2000) {
        $('.home-right').addClass("home-contain");
    }
    if (slider_caption <= 1024) {
        $('.home-right').addClass("home-contain");
    }

    /*----------------------------------------
     GO to Home
    ----------------------------------------*/
    $(window).on('scroll', function () {
        if ($(this).scrollTop() > 500) {
            $('.tap-top').fadeIn();
        } else {
            $('.tap-top').fadeOut();
        }
    });
    $('.tap-top').on('click', function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

    /*----------------------------------------
     color picker start
     ----------------------------------------*/
    // (function() {
    //     $('<div class="color-picker" id="theme">' +
    //         '<a href="#" class="handle">' +
    //         '<i class="fa fa-cog"></i>' +
    //         '</a><div class="sec-position"><div class="settings-header">' +
    //         '<h3>Select Color:</h3>' +
    //         '</div>' +
    //         '<div class="section">' +
    //         '<div class="colors o-auto">' +
    //         '<a href="#" class="color-1" ></a>' +
    //         '<a href="#" class="color-2" ></a>' +
    //         '<a href="#" class="color-3" ></a>' +
    //         '</div>' +
    //         '</div>' +
    //         '</div>' +
    //         '</div>').appendTo($('body'));
    // })();
    var body_event = $("body");
    body_event.on("click", ".color-1", function () {
        var link = $("<link />", {
            rel: "stylesheet",
            type: "text/css",
            href: "assets/css/color/color-1.css"
        });

        $('#color').html(link);
        $('#color-admin').html(link);
        return false;
    });
    body_event.on("click", ".color-2", function () {
        var link = $("<link />", {
            rel: "stylesheet",
            type: "text/css",
            href: "assets/css/color/color-2.css"
        });
        $('#color').html(link);
        $('#color-admin').html(link);
        return false;
    });
    body_event.on("click", ".color-3", function () {
        var link = $("<link />", {
            rel: "stylesheet",
            type: "text/css",
            href: "assets/css/color/color-3.css"
        });
        $('#color').html(link);
        $('#color-admin').html(link);
        return false;
    });
    $('.color-picker').animate({ right: '-190px' });
    body_event.on("click", ".color-picker a.handle", function (e) {
        e.preventDefault();
        var div = $('.color-picker');
        if (div.css('right') === '-190px') {
            $('.color-picker').animate({ right: '0px' });
        } else {
            $('.color-picker').animate({ right: '-190px' });
        }
    });

});